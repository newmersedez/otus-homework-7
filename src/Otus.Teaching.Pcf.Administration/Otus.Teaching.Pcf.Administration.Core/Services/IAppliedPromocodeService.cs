﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public interface IAppliedPromocodeService
    {
        Task<bool> UpdateAsync(Guid id);
    }

}