﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public class AppliedPromocodeService : IAppliedPromocodeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public AppliedPromocodeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<bool> UpdateAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null) return false;

            employee.AppliedPromocodesCount++;
            await _employeeRepository.UpdateAsync(employee);

            return true;
        }

    }
}