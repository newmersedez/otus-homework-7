﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Services;
using Otus.Teaching.Pcf.Administration.Integration.Dto;

namespace Otus.Teaching.Pcf.Administration.Integration.EventHandlers
{
    public class AdminEventHandler : IAdminEventHandler
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public AdminEventHandler(
            IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory ?? throw new ArgumentNullException(nameof(scopeFactory));
        }
        
        public async Task UpdatePromoCodesEvent(string message)
        {
            using var scope = _scopeFactory.CreateScope();
            var employeeRepository = scope.ServiceProvider.GetRequiredService<IRepository<Employee>>();
            var appliedPromocodeService = scope.ServiceProvider.GetRequiredService<IAppliedPromocodeService>();

            var adminNotified = JsonSerializer.Deserialize<NotifyAdminAboutPartnerManagerPromoCodeDto>(message);
            if (adminNotified?.PartnerManagerId == null)
            {
                return;
            }
            var adminId = adminNotified.PartnerManagerId.Value;
            var logger = scope.ServiceProvider.GetRequiredService<ILogger<AdminEventHandler>>();

            try
            {
                await appliedPromocodeService.UpdateAsync(adminId);
            }
            catch (Exception e)
            {
                logger.LogError($"Ошибка при обновлении промокода {e.Message}");
                throw;
            }
        }
    }
}