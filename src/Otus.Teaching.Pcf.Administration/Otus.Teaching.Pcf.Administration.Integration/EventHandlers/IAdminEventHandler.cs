﻿using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Integration.EventHandlers
{
    public interface IAdminEventHandler
    {
        Task UpdatePromoCodesEvent(string message);
    }
}