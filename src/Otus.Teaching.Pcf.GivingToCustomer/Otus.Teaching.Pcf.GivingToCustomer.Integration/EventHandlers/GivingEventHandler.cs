﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services.Promocodes;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration.EventHandlers
{
    public class GivingEventHandler : IGivingEventHandler
    {
        private readonly IServiceScopeFactory _scopeFactory;
        
        public GivingEventHandler(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task GivePromoCodeToCustomer(string message)
        {
            using var scope = _scopeFactory.CreateScope();
            var givenPromocodeService = scope.ServiceProvider.GetRequiredService<IGivenPromocodeService>();
            var logger = scope.ServiceProvider.GetRequiredService<ILogger<GivingEventHandler>>();

            try
            {
                var eventDto = JsonSerializer.Deserialize<GivePromoCodeToCustomerDto>(message);
                await givenPromocodeService.GivePromoCodesToCustomersWithPreference(eventDto);

            }
            catch (Exception e)
            {
                logger.LogError($"Ошибка при добавлении промокодов пользователям: {e.Message}");
                throw;
            }
        }
    }
}