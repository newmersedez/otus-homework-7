﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.GivingToCustomer.Integration.EventHandlers;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class GivingEventConsumer : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly IGivingEventHandler _eventHandler;
        private IConnection _connection;
        private IModel _channel;
        private const string QueueGivePromoCodeToCustomer = "GivePromoCodeToCustomer";

        public GivingEventConsumer(
            IConfiguration configuration,
            IGivingEventHandler eventHandler)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _eventHandler = eventHandler ?? throw new ArgumentNullException(nameof(eventHandler));
            Initialize();
        }

        private void Initialize()
        {
            var factory = new ConnectionFactory
            {
                UserName = "rmuser",
                Password = "rmpassword",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            
            _channel.ExchangeDeclare(QueueGivePromoCodeToCustomer, type: ExchangeType.Fanout);
            
            _channel.QueueDeclare(queue: QueueGivePromoCodeToCustomer,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            _channel.QueueBind(queue: QueueGivePromoCodeToCustomer,
                exchange: QueueGivePromoCodeToCustomer,
                routingKey: "");
        }

        public override void Dispose()
        {
            if (_channel.IsOpen)
            {
                _channel.Close();
                _connection.Close();
            }

            base.Dispose();
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (sender, args) =>
            {
                var body = args.Body;
                var message = Encoding.UTF8.GetString(body.ToArray());

                _eventHandler.GivePromoCodeToCustomer(message);
            };

            _channel.BasicConsume(queue: QueueGivePromoCodeToCustomer,
                autoAck: true, consumer);

            return Task.CompletedTask;
        }
    }
}