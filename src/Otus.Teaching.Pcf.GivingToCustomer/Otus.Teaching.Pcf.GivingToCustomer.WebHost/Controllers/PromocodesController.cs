﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services.Promocodes;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IGivenPromocodeService _givenPromocodeService;

        public PromocodesController(
            IRepository<PromoCode> promoCodesRepository,
            IGivenPromocodeService givenPromocodeService)
        {
            _promoCodesRepository = promoCodesRepository;
            _givenPromocodeService = givenPromocodeService;
        }
        
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodesRepository.GetAllAsync();

            var response = promocodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerId = x.PartnerId,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var result = await _givenPromocodeService.GivePromoCodesToCustomersWithPreference(new GivePromoCodeToCustomerDto
            {
                ServiceInfo = request.ServiceInfo,
                PartnerId = request.PartnerId,
                PromoCodeId = request.PromoCodeId,
                PromoCode = request.PromoCode,
                PreferenceId = request.PreferenceId,
                BeginDate = request.BeginDate,
                EndDate = request.EndDate,

            });

            if (!result)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}