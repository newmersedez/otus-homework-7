﻿using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services.Utils;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services.Promocodes
{
    public class GivenPromocodeService : IGivenPromocodeService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Preference> _preferencesRepository;

        public GivenPromocodeService(
            IRepository<PromoCode> promoCodesRepository, 
            IRepository<Customer> customersRepository, 
            IRepository<Preference> preferencesRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _customersRepository = customersRepository;
            _preferencesRepository = preferencesRepository;
        }


        public async Task<bool> GivePromoCodesToCustomersWithPreference(GivePromoCodeToCustomerDto request)
        {
            var preference = await _preferencesRepository.GetByIdAsync(request.PreferenceId);

            if (preference is null) return false;

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);
            
            await _promoCodesRepository.AddAsync(promoCode);

            return true;
        }
    }
}