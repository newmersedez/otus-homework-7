﻿using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services.Promocodes
{
    public interface IGivenPromocodeService
    {
        Task<bool> GivePromoCodesToCustomersWithPreference(GivePromoCodeToCustomerDto request);
    }
}